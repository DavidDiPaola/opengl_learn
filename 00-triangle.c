/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

/* followed this tutorial: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-2-the-first-triangle/ */

#include <stdio.h>

#include <stdlib.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "ogl/ogl.h"

int
main() {
	GLFWwindow * window = ogl_init(400, 240, 1, "00 - triangle");

	/* vu: vertex shader uniform,  va: vertex shader attribute,  fu: fragment shader uniform,  fv: fragment shader varying */
	const char * program_vertex_source =
		"#version 100" "\n"
		"precision highp float;" "\n"
		"attribute vec3 va_vertex_position;" "\n"  /* attributes are inputs to vertex shaders */
		"void main(){" "\n"
		"	gl_Position = vec4(va_vertex_position,1);" "\n"
		"}" "\n"
	;
	const char * program_fragment_source =
		"#version 100" "\n"
		"precision lowp float;" "\n"
		"void main() {" "\n"
		"	gl_FragColor = vec4(1,0,0,1);" "\n"
		"}" "\n"
	;
	GLuint program_ID = ogl_program_build(program_vertex_source, program_fragment_source);
	GLint program_va_vertex_position_ID = glGetAttribLocation(program_ID, "va_vertex_position");

	const GLfloat triangle_vertexbuffer_data[] = { 
		-1.0f, -1.0f, 0.0f,   1.0f, -1.0f, 0.0f,   0.0f,  1.0f, 0.0f,
	};
	const size_t triangle_vertexbuffer_data_size     = sizeof(triangle_vertexbuffer_data);
	const size_t triangle_vertexbuffer_data_vertexes = (sizeof(triangle_vertexbuffer_data) / sizeof(*triangle_vertexbuffer_data)) / 3;
	GLuint triangle_vertexbuffer_ID = ogl_arraybuffer_load(triangle_vertexbuffer_data, triangle_vertexbuffer_data_size);

	do {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(program_ID);

		glEnableVertexAttribArray(program_va_vertex_position_ID);
		glBindBuffer(GL_ARRAY_BUFFER, triangle_vertexbuffer_ID);
		glVertexAttribPointer(program_va_vertex_position_ID, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
		glDrawArrays(GL_TRIANGLES, 0, triangle_vertexbuffer_data_vertexes);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(program_va_vertex_position_ID);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	while (
		(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
		&&
		(glfwWindowShouldClose(window) == 0)
	);

	glDeleteBuffers(1, &triangle_vertexbuffer_ID);
	glDeleteProgram(program_ID);

	glfwTerminate();

	return 0;
}

