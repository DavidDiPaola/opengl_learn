/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "ogl.h"

struct _test_values {
	const char * desc;
	struct ogl_mat4f a;
	struct ogl_mat4f b;
	struct ogl_mat4f correct;
};

static int fail = 0;

static void
_test(struct _test_values values) {
	printf("[TEST]");

	struct ogl_mat4f result;
	ogl_mat4f_multiply(values.a, values.b, &result);

	int pass = ogl_mat4f_isapproxequal(values.correct, result);
	if (pass) {
		printf("[ OK ] (%s)" "\n", values.desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values.desc);

		ogl_mat4f_print(values.a, "\t", NULL);

		printf("\t" "*" "\n");

		ogl_mat4f_print(values.b, "\t", NULL);

		printf("\t" "was" "\n");

		ogl_mat4f_print(result, "\t", NULL);

		printf("\t" "should be" "\n");

		ogl_mat4f_print(values.correct, "\t", NULL);
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{
			.desc="mat4f multiply() test 1",
			.a={.values={
				  0.0f,   1.0f,   2.0f,   3.0f,
				  4.0f,   5.0f,   6.0f,   7.0f,
				  8.0f,   9.0f,  10.0f,  11.0f,
				 12.0f,  13.0f,  14.0f,  15.0f,
			}},
			.b={.values={
				  0.0f,   1.0f,   2.0f,   3.0f,
				  4.0f,   5.0f,   6.0f,   7.0f,
				  8.0f,   9.0f,  10.0f,  11.0f,
				 12.0f,  13.0f,  14.0f,  15.0f,
			}},
			.correct={.values={
				 56.0f,  62.0f,  68.0f,  74.0f,
				152.0f, 174.0f, 196.0f, 218.0f,
				248.0f, 286.0f, 324.0f, 362.0f,
				344.0f, 398.0f, 452.0f, 506.0f,
			}},
		},
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(testvalues[i]);
	}

	if (fail) {
		return -1;
	}

	return 0;
}

