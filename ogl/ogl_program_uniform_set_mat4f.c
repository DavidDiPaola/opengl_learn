/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <GL/glew.h>

#include "ogl.h"

int
ogl_program_uniform_set_mat4f(
	GLint program_ID, const char * uniform_name, const struct ogl_mat4f matrix
) {
	GLint uniform_ID = 0;
	int status = ogl_program_uniform_get_ID(program_ID, uniform_name, &uniform_ID);
	if (status < 0) {
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		return status;
	}

	const GLfloat * v = matrix.values;
	const GLfloat   v_columnmajor[16] = {
		v[ 0], v[ 4], v[ 8], v[12],
		v[ 1], v[ 5], v[ 9], v[13],
		v[ 2], v[ 6], v[10], v[14],
		v[ 3], v[ 7], v[11], v[15],
	};
	glUniformMatrix4fv(uniform_ID, 1, GL_FALSE, v_columnmajor);

	return 0;
}

