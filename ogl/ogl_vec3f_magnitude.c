/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include <math.h>

#include "ogl.h"

GLfloat
ogl_vec3f_magnitude(
	struct ogl_vec3f vector
) {
	return sqrtf((vector.x*vector.x) + (vector.y*vector.y) + (vector.z*vector.z));
}

