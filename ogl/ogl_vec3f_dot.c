/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include "ogl.h"

void
ogl_vec3f_dot(
	struct ogl_vec3f a, struct ogl_vec3f b,
	GLfloat * out_result
) {
	/* from Wikipedia (https://en.wikipedia.org/wiki/Dot_product#Algebraic_definition) */
	(*out_result) = (a.x*b.x) + (a.y*b.y) + (a.z*b.z);
}

