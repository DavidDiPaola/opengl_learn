/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "ogl.h"

void
ogl_vec3f_print(
	struct ogl_vec3f vector
) {
	printf("{ ");

	printf(".x=");
	ogl_GLfloat_print(vector.x);
	printf(", ");

	printf(".y=");
	ogl_GLfloat_print(vector.y);
	printf(", ");

	printf(".z=");
	ogl_GLfloat_print(vector.z);

	printf(" }");
}


