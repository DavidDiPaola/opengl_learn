/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "ogl.h"

void
ogl_mat4f_print(
	struct ogl_mat4f matrix, const char * line_prefix, const char * line_suffix
) {
	line_prefix = (line_prefix == NULL) ? "" : line_prefix;
	line_suffix = (line_suffix == NULL) ? "" : line_suffix;

	printf("%s" "{" "%s" "\n", line_prefix, line_suffix);
	for (int i=0; i<16; i++) {
		if ((i%4) == 0) {
			printf("%s" "\t", line_prefix);
		}
		//printf("%8g" "f, ", matrix.values[i]);
		ogl_GLfloat_print(matrix.values[i]);
		printf(", ");
		if ((i%4) == 3) {
			printf("%s" "\n", line_suffix);
		}
	}
	printf("%s" "}" "%s" "\n", line_prefix, line_suffix);

}


