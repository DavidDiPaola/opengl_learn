/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include "ogl.h"

struct _test_values {
	const char * desc;
	struct ogl_vec3f a;
	struct ogl_vec3f b;
	struct ogl_vec3f correct;
};

static int fail = 0;

static void
_test(struct _test_values values) {
	printf("[TEST]");

	struct ogl_vec3f result;
	ogl_vec3f_cross(values.a, values.b, &result);

	int pass = ogl_vec3f_isapproxequal(values.correct, result);
	if (pass) {
		printf("[ OK ] (%s)" "\n", values.desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values.desc);

		printf("\t");
		ogl_vec3f_print(values.a);
		printf(" x ");
		ogl_vec3f_print(values.b);
		printf("\n");

		printf("\t" "was" "\n");

		printf("\t");
		ogl_vec3f_print(result);
		printf("\n");

		printf("\t" "should be" "\n");

		printf("\t");
		ogl_vec3f_print(values.correct);
		printf("\n");
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{ .desc="vec3f cross() test 1", .a={.x=1.0f, .y=2.0f, .z=3.0f}, .b={.x=4.0f, .y=5.0f, .z=6.0f}, .correct={.x=-3.0f, .y=6.0f, .z=-3.0f} },
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(testvalues[i]);
	}

	if (fail) {
		return -1;
	}

	return 0;
}

