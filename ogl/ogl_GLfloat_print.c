/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include <stdio.h>

#include <math.h>

#include "ogl.h"

void
ogl_GLfloat_print(
	GLfloat f
) {
	printf("%g", f);

	long int rounded = lroundf(f);
	if (ogl_GLfloat_isapproxequal((float)rounded, f)) {
		printf(".0");
	}

	printf("f");
}

