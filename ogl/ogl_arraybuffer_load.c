/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

GLuint
ogl_arraybuffer_load(const GLvoid * data, GLsizeiptr data_size) {
	GLuint buffer_ID;
	glGenBuffers(1, &buffer_ID);
	glBindBuffer(GL_ARRAY_BUFFER, buffer_ID);
	glBufferData(GL_ARRAY_BUFFER, data_size, data, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	return buffer_ID;
}

