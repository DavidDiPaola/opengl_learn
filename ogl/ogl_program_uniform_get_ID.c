/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <GL/glew.h>

int
ogl_program_uniform_get_ID(
	GLuint program_ID, const char * uniform_name,
	GLint * out_uniform_ID
) {
	GLint uniform_ID;

	uniform_ID = glGetUniformLocation(program_ID, uniform_name);
	if (uniform_ID < 0) {
		fprintf(stderr, "GL glGetUniformLocation(%i, \"%s\"): ERROR" "\n", program_ID, uniform_name);
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		return -1;
	}

	(*out_uniform_ID) = uniform_ID;
	return 0;
}

