/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef __OGL_H
#define __OGL_H

#include <GLFW/glfw3.h>

extern int          _ogl_window_width;
extern int          _ogl_window_height;

void
_ogl_glfw_error(int error, const char * description);

#endif

