/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <stdlib.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "ogl.h"
#include "_ogl.h"

GLFWwindow *
ogl_init(int window_width, int window_height, int vsync, const char * window_title) {
	GLFWwindow * window;
	int status;

	glfwSetErrorCallback(&_ogl_glfw_error);

	status = glfwInit();
	if (!status) {  /* TODO GLFW_TRUE doesn't exist..? */
		fprintf(stderr, "GLFW glfwInit(): ERROR" "\n");
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		exit(-1);
	}

	glfwWindowHint(GLFW_CLIENT_API,            GLFW_OPENGL_ES_API);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);

	window = glfwCreateWindow(window_width, window_height, window_title, NULL, NULL);
	if (window == NULL){
		fprintf(stderr, "GLFW glfwCreateWindow(): ERROR" "\n");
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		glfwTerminate();
		exit(-1);
	}

	glfwMakeContextCurrent(window);

	GLenum glewstatus;
	glewstatus = glewInit();
	if (glewstatus != GLEW_OK) {
		fprintf(stderr, "GLEW glewInit(): ERROR" "\n");
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		glfwTerminate();
		exit(-1);
	}

	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);

	if (!vsync) {
		glfwSwapInterval(0);
	}

	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);

	glEnable(GL_DEPTH_TEST);
	glDepthFunc(GL_LESS);

	_ogl_window_width  = window_width;
	_ogl_window_height = window_height;
	return window;
}

