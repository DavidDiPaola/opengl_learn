/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include "ogl.h"

int
ogl_vec3f_normal(
	struct ogl_vec3f vector,
	struct ogl_vec3f * out_result
) {
	GLfloat vector_magnitude = ogl_vec3f_magnitude(vector);
	if (ogl_GLfloat_isapproxequal(vector_magnitude, 0.0f)) {
		return -1;
	}

	(*out_result).x = vector.x / vector_magnitude;
	(*out_result).y = vector.y / vector_magnitude;
	(*out_result).z = vector.z / vector_magnitude;
	return 0;
}

