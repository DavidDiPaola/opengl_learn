/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <stdlib.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

static int
_shader_compile(
	GLenum shader_type, const char * shader_code,
	GLuint * out_shader_ID
) {
	GLuint shader_ID;
	const char * shader_type_str = (shader_type == GL_VERTEX_SHADER) ? "vertex" : "fragment";

	shader_ID = glCreateShader(shader_type);

	glShaderSource(shader_ID, 1, &shader_code, NULL);

	glCompileShader(shader_ID);
	GLsizei log_length = 0;
	glGetShaderiv(shader_ID, GL_INFO_LOG_LENGTH, &log_length);
	if (log_length > 0) {
		GLchar * log = calloc(log_length, sizeof(GLchar));  /* TODO handle error */
		glGetShaderInfoLog(shader_ID, log_length, NULL, log);
		fprintf(stderr, "GL %s shader compiler: %s" "\n", shader_type_str, log);
		free(log);
	}

	GLint status_compile = GL_FALSE;
	glGetShaderiv(shader_ID, GL_COMPILE_STATUS, &status_compile);
	if (status_compile != GL_TRUE) {
		fprintf(stderr, "GL %s shader compiler: ERROR" "\n", shader_type_str);
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		return -1;
	}

	(*out_shader_ID) = shader_ID;
	return 0;
}

GLuint
ogl_program_build(const char * program_vertex_source, const char * program_fragment_source) {
	int status;

	GLuint vertexshader_ID;
	status = _shader_compile(GL_VERTEX_SHADER, program_vertex_source, &vertexshader_ID);
	if (status < 0) {
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		exit(-1);
	}

	GLuint fragmentshader_ID;
	status = _shader_compile(GL_FRAGMENT_SHADER, program_fragment_source, &fragmentshader_ID);
	if (status < 0) {
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		exit(-1);
	}

	GLuint program_ID = glCreateProgram();
	glAttachShader(program_ID, vertexshader_ID);
	glAttachShader(program_ID, fragmentshader_ID);
	glLinkProgram(program_ID);
	GLsizei log_length = 0;
	glGetProgramiv(program_ID, GL_INFO_LOG_LENGTH, &log_length);
	if ( log_length > 0 ){
		GLchar * log = calloc(log_length, sizeof(GLchar));  /* TODO handle error */
		glGetProgramInfoLog(program_ID, log_length, NULL, log);
		fprintf(stderr, "GL shader program linker: %s" "\n", log);
		free(log);
	}
	GLint status_compile = GL_FALSE;
	glGetProgramiv(program_ID, GL_LINK_STATUS, &status_compile);
	if (status_compile != GL_TRUE) {
		fprintf(stderr, "GL shader program linker: ERROR" "\n");
		fprintf(stderr, "\t" "at %s : %d" "\n", __FILE__, __LINE__);
		exit(-1);
	}

	glDetachShader(program_ID, vertexshader_ID);
	glDetachShader(program_ID, fragmentshader_ID);
	
	glDeleteShader(vertexshader_ID);
	glDeleteShader(fragmentshader_ID);

	return program_ID;
}

