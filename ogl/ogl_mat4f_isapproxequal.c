/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include "ogl.h"

int
ogl_mat4f_isapproxequal(
	struct ogl_mat4f a, struct ogl_mat4f b
) {
	int result = 1;

	for (size_t i=0; i<16; i++) {
		result &= (ogl_GLfloat_isapproxequal(a.values[i], b.values[i]));
	}

	return result;
}

