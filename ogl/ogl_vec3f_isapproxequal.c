/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "ogl.h"

int
ogl_vec3f_isapproxequal(
	struct ogl_vec3f a, struct ogl_vec3f b
) {
	return (
		ogl_GLfloat_isapproxequal(a.x, b.x)
		&&
		ogl_GLfloat_isapproxequal(a.y, b.y)
		&&
		ogl_GLfloat_isapproxequal(a.z, b.z)
	);
}

