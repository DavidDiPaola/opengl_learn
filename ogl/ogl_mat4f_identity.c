/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include "ogl.h"

void
ogl_mat4f_identity(
	struct ogl_mat4f * out_matrix
) {
	GLfloat * values = (*out_matrix).values;
	values[ 0] = 1.0f; values[ 1] = 0.0f; values[ 2] = 0.0f; values[ 3] = 0.0f;
	values[ 4] = 0.0f; values[ 5] = 1.0f; values[ 6] = 0.0f; values[ 7] = 0.0f;
	values[ 8] = 0.0f; values[ 9] = 0.0f; values[10] = 1.0f; values[11] = 0.0f;
	values[12] = 0.0f; values[13] = 0.0f; values[14] = 0.0f; values[15] = 1.0f;
}

