/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "ogl.h"

void
ogl_mat4f_scale(
	struct ogl_mat4f matrix, struct ogl_vec3f scale,
	struct ogl_mat4f * out_result
) {
	/* see OpenGL 2.1 glScale(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glScale.xml */
	/* note: glScale() was deprecated, then removed from later OpenGL versions. don't use it */

	struct ogl_mat4f scale_matrix = { .values = {
		scale.x, 0.0f,    0.0f,    0.0f,
		0.0f,    scale.y, 0.0f,    0.0f,
		0.0f,    0.0f,    scale.z, 0.0f,
		0.0f,    0.0f,    0.0f,    1.0f,
	}};
	ogl_mat4f_multiply(matrix, scale_matrix, out_result);
}

