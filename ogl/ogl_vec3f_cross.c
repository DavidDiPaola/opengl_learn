/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include "ogl.h"

void
ogl_vec3f_cross(
	struct ogl_vec3f a, struct ogl_vec3f b,
	struct ogl_vec3f * out_result
) {
	GLfloat a_x = a.x, a_y = a.y, a_z = a.z;
	GLfloat b_x = b.x, b_y = b.y, b_z = b.z;

	/* from Wikipedia (https://en.wikipedia.org/wiki/Cross_product#Coordinate_notation) */
	(*out_result).x = (a_y*b_z) - (a_z*b_y);
	(*out_result).y = (a_z*b_x) - (a_x*b_z);
	(*out_result).z = (a_x*b_y) - (a_y*b_x);
}

