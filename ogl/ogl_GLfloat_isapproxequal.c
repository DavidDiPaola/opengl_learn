/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <GL/glew.h>

#include <math.h>

#include <float.h>

int
ogl_GLfloat_isapproxequal(
	GLfloat a, GLfloat b
) {
	return (fabsf(a - b) < 0.000001f);
}

