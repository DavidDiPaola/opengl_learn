/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <GLFW/glfw3.h>

#include "_ogl.h"

GLFWwindow * _ogl_window        = NULL;
int          _ogl_window_width  = 0;
int          _ogl_window_height = 0;

void
_ogl_glfw_error(int error, const char * description) {
	fprintf(stderr, "GLFW ERROR: (%i) %s" "\n", error, description);
}

