/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#ifndef _OGL_H
#define _OGL_H

#include <GL/glew.h>

#include <GLFW/glfw3.h>



struct ogl_mat4f {
	GLfloat values[4*4];
};

struct ogl_vec3f {
	GLfloat x;
	GLfloat y;
	GLfloat z;
};

struct ogl_quat {
	GLfloat angle;
	struct ogl_vec3f axis;
};



GLFWwindow *
ogl_init(int window_width, int window_height, int vsync, const char * window_title);



GLuint
ogl_program_build(const char * program_vertex_source, const char * program_fragment_source);

int
ogl_program_uniform_get_ID(
	GLuint programID, const char * uniform_name,
	GLint * out_uniform_ID
);

int
ogl_program_uniform_set_mat4f(
        GLint program_ID, const char * uniform_name, const struct ogl_mat4f matrix
);



GLuint
ogl_arraybuffer_load(const GLvoid * data, GLsizeiptr data_size);



int
ogl_GLfloat_isapproxequal(
	GLfloat a, GLfloat b
);

void
ogl_GLfloat_print(
	GLfloat f
);



void
ogl_mat4f_identity(
        struct ogl_mat4f * out_matrix
);

int
ogl_mat4f_isapproxequal(
	struct ogl_mat4f a, struct ogl_mat4f b
);

void
ogl_mat4f_multiply(
        struct ogl_mat4f a, struct ogl_mat4f b,
	struct ogl_mat4f * out_c
);

void
ogl_mat4f_print(
	struct ogl_mat4f matrix, const char * line_prefix, const char * line_suffix
);

void
ogl_mat4f_rotate(
	struct ogl_mat4f matrix, struct ogl_quat rotation,
	struct ogl_mat4f * out_result
);

void
ogl_mat4f_scale(
	struct ogl_mat4f matrix, struct ogl_vec3f scale,
	struct ogl_mat4f * out_result
);

void
ogl_mat4f_translate(
	struct ogl_mat4f matrix, struct ogl_vec3f translation,
	struct ogl_mat4f * out_result
);



void
ogl_vec3f_cross(
	struct ogl_vec3f a, struct ogl_vec3f b,
	struct ogl_vec3f * out_result
);

void
ogl_vec3f_dot(
	struct ogl_vec3f a, struct ogl_vec3f b,
	GLfloat * out_result
);

int
ogl_vec3f_isapproxequal(
	struct ogl_vec3f a, struct ogl_vec3f b
);

GLfloat
ogl_vec3f_magnitude(
	struct ogl_vec3f vector
);

int
ogl_vec3f_normal(
	struct ogl_vec3f vector,
	struct ogl_vec3f * out_result
);

void
ogl_vec3f_print(
	struct ogl_vec3f vector
);



void
ogl_lookat(
	struct ogl_vec3f eye, struct ogl_vec3f center, struct ogl_vec3f up,
	struct ogl_mat4f * out_matrix
);

void
ogl_perspective(
	GLfloat fovy, GLfloat zNear, GLfloat zFar,
	struct ogl_mat4f * out_matrix
);

#endif

