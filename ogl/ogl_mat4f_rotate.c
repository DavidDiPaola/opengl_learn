/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#define _ISOC99_SOURCE
#include <math.h>

#include "ogl.h"

void
ogl_mat4f_rotate(
	struct ogl_mat4f matrix, struct ogl_quat rotation,
	struct ogl_mat4f * out_result
) {
	/* see OpenGL 2.1 glRotate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glRotate.xml */
	/* note: glRotate() was deprecated, then removed from later OpenGL versions. don't use it */

	struct ogl_vec3f axis;
	ogl_vec3f_normal(rotation.axis, &axis);
	GLfloat x = rotation.axis.x;
	GLfloat y = rotation.axis.y;
	GLfloat z = rotation.axis.z;
	GLfloat c = cosf(rotation.angle);
	GLfloat s = sinf(rotation.angle);
	struct ogl_mat4f rotation_matrix = { .values = {
		(x*x*(1-c))+(  c), (x*y*(1-c))-(z*s), (x*z*(1-c))+(y*s), 0.0f,
		(y*x*(1-c))+(z*s), (y*y*(1-c))+(  c), (y*z*(1-c))-(x*s), 0.0f,
		(x*z*(1-c))-(y*s), (y*z*(1-c))+(x*s), (z*z*(1-c))+(  c), 0.0f,
		             0.0f,              0.0f,              0.0f, 1.0f,
	}};
	ogl_mat4f_multiply(matrix, rotation_matrix, out_result);
}

