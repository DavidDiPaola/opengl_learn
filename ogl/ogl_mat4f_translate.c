/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include "ogl.h"

void
ogl_mat4f_translate(
	struct ogl_mat4f matrix, struct ogl_vec3f translation,
	struct ogl_mat4f * out_result
) {
	/* see OpenGL 2.1 glTranslate(): https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/glTranslate.xml */
	/* note: glTranslate() was deprecated, then removed from later OpenGL versions. don't use it */

	struct ogl_mat4f translation_matrix = { .values = {
		1.0f, 0.0f, 0.0f, translation.x,
		0.0f, 1.0f, 0.0f, translation.y,
		0.0f, 0.0f, 1.0f, translation.z,
		0.0f, 0.0f, 0.0f,          1.0f,
	}};
	ogl_mat4f_multiply(matrix, translation_matrix, out_result);
}

