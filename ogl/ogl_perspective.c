/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <stdlib.h>

#include <GL/glew.h>

#include <math.h>

#include "ogl.h"
#include "_ogl.h"

void
ogl_perspective(
	GLfloat fovy, GLfloat zNear, GLfloat zFar,
	struct ogl_mat4f * out_matrix
) {
	/* from https://www.khronos.org/registry/OpenGL-Refpages/gl2.1/xhtml/gluPerspective.xml */

	GLfloat aspect = (GLfloat)_ogl_window_width / (GLfloat)_ogl_window_height;

	if (fovy < 0.0f) {
		fprintf(stderr, "ogl:perspective() ERROR: fovy < 0.0f (is %g)" "\n", fovy);
		exit(-1);
	}
	if (aspect < 0.0f) {
		fprintf(stderr, "ogl:perspective() ERROR: aspect < 0.0f (is %g)" "\n", aspect);
		exit(-1);
	}
	if (zNear < 0.0f) {
		fprintf(stderr, "ogl:perspective() ERROR: zNear < 0.0f (is %g)" "\n", zNear);
		exit(-1);
	}
	if (zFar < 0.0f) {
		fprintf(stderr, "ogl:perspective() ERROR: zFar < 0.0f (is %g)" "\n", zFar);
		exit(-1);
	}
	if (zNear > zFar) {
		fprintf(stderr, "ogl:perspective() ERROR: zNear > zFar (%g > %g)" "\n", zNear, zFar);
		exit(-1);
	}

	GLfloat f = 1.0f / tanf(fovy / 2.0f);
	
	GLfloat * values = (*out_matrix).values;
	values[ 0] = f / aspect;    values[ 1] = 0.0f;    values[ 2] =  0.0f;                          values[ 3] = 0.0f;
	values[ 4] = 0.0f;          values[ 5] = f;       values[ 6] =  0.0f;                          values[ 7] = 0.0f;
	values[ 8] = 0.0f;          values[ 9] = 0.0f;    values[10] = (zFar+zNear) / (zNear-zFar);    values[11] = (2*zFar*zNear) / (zNear-zFar);
	values[12] = 0.0f;          values[13] = 0.0f;    values[14] = -1.0f;                          values[15] = 0.0f;
}

