/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

#include <stdio.h>

#include <GL/glew.h>

#include "ogl.h"

struct _test_values {
	const char * desc;
	struct ogl_vec3f a;
	struct ogl_vec3f correct;
};

static int fail = 0;

static void
_test(struct _test_values values) {
	printf("[TEST]");

	struct ogl_vec3f output;
	int status = ogl_vec3f_normal(values.a, &output);
	int pass_status = (status >= 0);
	int pass_output = ogl_vec3f_isapproxequal(output, values.correct);
	if (pass_status && pass_output) {
		printf("[ OK ] (%s)" "\n", values.desc);
	}
	else {
		fail = 1;

		printf("[FAIL] (%s)" "\n", values.desc);

		if (!pass_status) {
			printf("\t" "status FAIL: %i" "\n", status);
		}
		else {
			printf("\t" "status OK: %i" "\n", status);
		}

		if (!pass_output) {
			printf("\t" "output FAIL:" "\n");

			printf("\t\t" "|| ");
			ogl_vec3f_print(values.a);
			printf(" ||" "\n");

			printf("\t\t" "was" "\n");

			printf("\t\t");
			ogl_vec3f_print(output);
			printf("\n");

			printf("\t\t" "should be" "\n");

			printf("\t\t");
			ogl_vec3f_print(values.correct);
			printf("\n");
		}
		else {
			printf("\t" "output OK" "\n");
		}
	}
}

int
main() {
	struct _test_values testvalues[] = {
		{ .desc="vec3f normal() test 1", .a={.x=1.0f, .y=2.0f, .z=3.0f}, .correct={.x=0.267261f, .y=0.534522f, .z=0.801784f}},
	};
	size_t testvalues_length = sizeof(testvalues) / sizeof(*testvalues);
	for (size_t i=0; i<testvalues_length; i++) {
		_test(testvalues[i]);
	}

	if (fail) {
		return -1;
	}

	return 0;
}

