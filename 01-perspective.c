/*
2018 David DiPaola
licensed under CC0 (public domain, see https://creativecommons.org/publicdomain/zero/1.0/)
*/

/* followed this tutorial: http://www.opengl-tutorial.org/beginners-tutorials/tutorial-3-matrices/ */

#include <stdio.h>

#include <stdlib.h>

#include <GL/glew.h>

#include <GLFW/glfw3.h>

#include "ogl/ogl.h"

int
main() {
	GLFWwindow * window = ogl_init(400, 240, 1, "01 - triangle with perspective");

	/* vu: vertex shader uniform,  va: vertex shader attribute,  fu: fragment shader uniform,  fv: fragment shader varying */
	const char * program_vertex_source =
		"#version 100" "\n"
		"precision highp float;" "\n"
		"uniform   mat4 vu_vertex_MVP;" "\n"       /* uniforms are inputs to vertex and fragment shaders that don't change per-vertex */
		"attribute vec3 va_vertex_position;" "\n"  /* attributes are inputs to vertex shaders */
		"void main(){" "\n"
		"	gl_Position = vu_vertex_MVP * vec4(va_vertex_position,1);" "\n"
		"}" "\n"
	;
	const char * program_fragment_source =
		"#version 100" "\n"
		"precision lowp float;" "\n"
		"void main() {" "\n"
		"	gl_FragColor = vec4(1,0,0,1);" "\n"
		"}" "\n"
	;
	GLuint program_ID = ogl_program_build(program_vertex_source, program_fragment_source);
	GLint program_va_vertex_position_ID = glGetAttribLocation(program_ID, "va_vertex_position");

	struct ogl_mat4f MVP_projection;
	ogl_perspective(45.0f, 0.1f, 100.0f, &MVP_projection);

	struct ogl_mat4f MVP_view;
	struct ogl_vec3f MVP_view_eye    = { .x= 4.0f,  .y= 3.0f,  .z= 3.0f };
	struct ogl_vec3f MVP_view_center = { .x= 0.0f,  .y= 0.0f,  .z= 0.0f };
	struct ogl_vec3f MVP_view_up     = { .x= 0.0f,  .y= 1.0f,  .z= 0.0f };
	ogl_lookat(MVP_view_eye, MVP_view_center, MVP_view_up, &MVP_view);

	struct ogl_mat4f MVP_model;
	ogl_mat4f_identity(&MVP_model);

	struct ogl_mat4f MVP;
	ogl_mat4f_identity(&MVP);
	ogl_mat4f_multiply(MVP, MVP_projection, &MVP);
	ogl_mat4f_multiply(MVP, MVP_view,       &MVP);
	ogl_mat4f_multiply(MVP, MVP_model,      &MVP);
	
	const GLfloat triangle_vertexbuffer_data[] = {
		-1.0f, -1.0f, 0.0f,   1.0f, -1.0f, 0.0f,   0.0f,  1.0f, 0.0f,
	};
	const size_t triangle_vertexbuffer_data_size     = sizeof(triangle_vertexbuffer_data);
	const size_t triangle_vertexbuffer_data_vertexes = (sizeof(triangle_vertexbuffer_data) / sizeof(*triangle_vertexbuffer_data)) / 3;
	GLuint triangle_vertexbuffer_ID = ogl_arraybuffer_load(triangle_vertexbuffer_data, triangle_vertexbuffer_data_size);

	do {
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		glUseProgram(program_ID);
		ogl_program_uniform_set_mat4f(program_ID, "vu_vertex_MVP", MVP);

		glEnableVertexAttribArray(program_va_vertex_position_ID);
		glBindBuffer(GL_ARRAY_BUFFER, triangle_vertexbuffer_ID);
		glVertexAttribPointer(program_va_vertex_position_ID, 3, GL_FLOAT, GL_FALSE, 0, (GLvoid *)0);
		glDrawArrays(GL_TRIANGLES, 0, triangle_vertexbuffer_data_vertexes);
		glBindBuffer(GL_ARRAY_BUFFER, 0);
		glDisableVertexAttribArray(program_va_vertex_position_ID);

		glfwSwapBuffers(window);
		glfwPollEvents();
	}
	while (
		(glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS)
		&&
		(glfwWindowShouldClose(window) == 0)
	);

	glDeleteBuffers(1, &triangle_vertexbuffer_ID);
	glDeleteProgram(program_ID);

	glfwTerminate();

	return 0;
}

