OGL_BIN = ogl/ogl.a

00_SRC = 00-triangle.c 
00_OBJ = $(00_SRC:.c=.o) $(OGL_BIN)
00_BIN = 00-triangle

01_SRC = 01-perspective.c
01_OBJ = $(01_SRC:.c=.o) $(OGL_BIN)
01_BIN = 01-perspective

02_SRC = 02-cube.c
02_OBJ = $(02_SRC:.c=.o) $(OGL_BIN)
02_BIN = 02-cube

03_SRC = 03-texture.c
03_OBJ = $(03_SRC:.c=.o) $(OGL_BIN)
03_BIN = 03-texture

04_SRC = 04-lighting.c
04_OBJ = $(04_SRC:.c=.o) $(OGL_BIN)
04_BIN = 04-lighting

05_SRC = 05-geometrytransforms.c
05_OBJ = $(05_SRC:.c=.o) $(OGL_BIN)
05_BIN = 05-geometrytransforms

06_SRC = 06-performance.c
06_OBJ = $(06_SRC:.c=.o) $(OGL_BIN)
06_BIN = 06-performance



CFLAGS ?= -std=c99 -Wall -fwrapv -g
LIB_CFLAGS  += $(shell pkg-config --cflags glew gl glfw3)
LIB_LDFLAGS += $(shell pkg-config --libs   glew gl glfw3) -lm

.PHONY: all
all: $(00_BIN) $(01_BIN) $(02_BIN) $(03_BIN) $(04_BIN) $(05_BIN) $(06_BIN)

.PHONY: clean
clean:
	rm -rf \
		$(00_OBJ) $(00_BIN) \
		$(01_OBJ) $(01_BIN) \
		$(02_OBJ) $(02_BIN) \
		$(03_OBJ) $(03_BIN) \
		$(04_OBJ) $(04_BIN) \
		$(05_OBJ) $(05_BIN) \
		$(06_OBJ) $(06_BIN)
	$(MAKE) --directory=./ogl/ clean

$(00_BIN): $(00_OBJ)

$(01_BIN): $(01_OBJ)

$(02_BIN): $(02_OBJ)

$(03_BIN): $(03_OBJ)

$(04_BIN): $(04_OBJ)

$(05_BIN): $(05_OBJ)

$(06_BIN): $(06_OBJ)

.PHONY: $(OGL_BIN)
$(OGL_BIN):
	$(MAKE) --directory=./ogl/ all

%.o: %.c
	@echo [CC] $<
	@$(CC) $(CFLAGS) $(LIB_CFLAGS) -c $< -o $@

%: %.o
	@echo [LD] $^ -o $@
	@$(CC) $(LDFLAGS) $^ $(OGL_BIN) -o $@ $(LIB_LDFLAGS)

